package com.example.javaassignment2employeeapi.models.dto;

import java.util.ArrayList;

public class EmployeeHiredLastTwoYearsDto {

    private ArrayList<String> employeesHiredTwoYears;

    public EmployeeHiredLastTwoYearsDto(ArrayList<String> employeesHiredTwoYears) {
        this.employeesHiredTwoYears = employeesHiredTwoYears;
    }


    public ArrayList<String> getEmployeesHiredTwoYears() {
        return employeesHiredTwoYears;
    }

    public void setEmployeesHiredTwoYears(ArrayList<String> employeesHiredTwoYears) {
        this.employeesHiredTwoYears = employeesHiredTwoYears;
    }
}
