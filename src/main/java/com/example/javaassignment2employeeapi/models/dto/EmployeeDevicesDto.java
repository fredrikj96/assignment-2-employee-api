package com.example.javaassignment2employeeapi.models.dto;


import java.util.ArrayList;


public class EmployeeDevicesDto {
 private ArrayList<String> totalDevices;


 public ArrayList<String> getTotalDevices() {
  return totalDevices;
 }

 public void setTotalDevices(ArrayList<String> totalDevices) {
  this.totalDevices = totalDevices;
 }

 public EmployeeDevicesDto(ArrayList<String> totalDevices) {
  this.totalDevices = totalDevices;
 }


}