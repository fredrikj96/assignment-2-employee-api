package com.example.javaassignment2employeeapi.models.dto;

import java.util.ArrayList;

public class EmployeeHappyBirthdayDto {
   private ArrayList<String> fulltimeHappyBirthday;

    public EmployeeHappyBirthdayDto(ArrayList<String> fulltimeHappyBirthday) {
        this.fulltimeHappyBirthday = fulltimeHappyBirthday;
    }

    public ArrayList<String> getFulltimeHappyBirthday() {
        return fulltimeHappyBirthday;
    }

    public void setFulltimeHappyBirthday(ArrayList<String> fulltimeHappyBirthday) {
        this.fulltimeHappyBirthday = fulltimeHappyBirthday;
    }
}
