package com.example.javaassignment2employeeapi.models.domain;

public class LuhnValidator {

    public static boolean luhnValidator(String input) {

        if (input.length() != 10) {
            return false;
        }

        int addOdd = addOddCount(input);
        int addEven = addEvenCount(input);

        return (addEven + addOdd) % 10 == 0;
    }


    protected static int addEvenCount(String input) {
        int inputNumLength = input.length();
        int addEven = 0;

        for (int i = inputNumLength - 2; i >= 0; i = i - 2) {
            int num = input.charAt(i) - '0';

            int numTwo = num * 2;

            if (numTwo > 9) {
                numTwo = numTwo % 10 + numTwo / 10;
            }

            addEven += numTwo;
        }
        return addEven;
    }


    protected static int addOddCount(String input) {
        int inputNumLength = input.length();
        int addOdd = 0;

        for (int i = inputNumLength - 1; i >= 1; i = i - 2) {
            int num = input.charAt(i) - '0';

            addOdd += num;
        }

        return addOdd;
    }


}
