package com.example.javaassignment2employeeapi.models.domain;


import java.util.ArrayList;

public class Employee {

    int id;
    String firstName;
    String lastName;
    String dateOfBirth;
    String employmentStatus;
    String position;
    String dateEmployed;
    ArrayList<String> currentDevices;
    String employeeCode;


    public Employee(int id, String firstName, String lastName, String dateOfBirth, String employmentStatus, String position, String dateEmployed, ArrayList<String> currentDevices, String employeeCode) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.employmentStatus = employmentStatus;
        this.position = position;
        this.dateEmployed = dateEmployed;
        this.currentDevices = currentDevices;
        this.employeeCode = employeeCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDateEmployed() {
        return dateEmployed;
    }

    public void setDateEmployed(String dateEmployed) {
        this.dateEmployed = dateEmployed;
    }

    public ArrayList<String> getCurrentDevices() {
        return currentDevices;
    }

    public void setCurrentDevices(ArrayList<String>  currentDevices) {
        this.currentDevices = currentDevices;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }
}
