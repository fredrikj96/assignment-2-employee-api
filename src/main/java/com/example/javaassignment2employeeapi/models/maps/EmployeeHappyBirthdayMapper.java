package com.example.javaassignment2employeeapi.models.maps;

import com.example.javaassignment2employeeapi.models.domain.Employee;
import com.example.javaassignment2employeeapi.models.dto.EmployeeHappyBirthdayDto;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EmployeeHappyBirthdayMapper {


    private static String daysUntilNextBday (String bDay) {

        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate currentDate = LocalDate.now();
        LocalDate bDayFulltime = LocalDate.parse(bDay, timeFormatter);
        LocalDate nextBirthdayDate = bDayFulltime.withYear(currentDate.getYear());

        if(nextBirthdayDate.isBefore(currentDate)) {
            nextBirthdayDate = nextBirthdayDate.plusYears(1);
        }

        int numOfDaysUntilBday = (int) ChronoUnit.DAYS.between(currentDate, nextBirthdayDate);

        return String.valueOf(numOfDaysUntilBday);

    }

    public static EmployeeHappyBirthdayDto employeeHappyBirthdayDtoMap(HashMap<Integer, Employee> employeeHashMapInput) {

        ArrayList<String> happyBirthdayFulltimeMap = new ArrayList<>();

        for (Map.Entry<Integer, Employee> entry: employeeHashMapInput.entrySet()) {
            String firstName = entry.getValue().getFirstName();
            String lastName = entry.getValue().getLastName();
            String dateOfBirth = entry.getValue().getDateOfBirth();
            String statusFulltime = entry.getValue().getEmploymentStatus();

            if (statusFulltime.contains("Full-time")) {

                happyBirthdayFulltimeMap.add(firstName + " " + lastName);
                happyBirthdayFulltimeMap.add("Birthday is: " + dateOfBirth);
                happyBirthdayFulltimeMap.add("Days until next birthday is: " + daysUntilNextBday(dateOfBirth));
            }
        }

        return new EmployeeHappyBirthdayDto(happyBirthdayFulltimeMap);
    }

}
