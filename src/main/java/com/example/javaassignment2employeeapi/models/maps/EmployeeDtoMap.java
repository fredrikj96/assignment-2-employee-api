package com.example.javaassignment2employeeapi.models.maps;


import com.example.javaassignment2employeeapi.models.domain.Employee;
import com.example.javaassignment2employeeapi.models.dto.EmployeeDevicesDto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EmployeeDtoMap {

    public static EmployeeDevicesDto employeeDevicesDtoMap(HashMap<Integer, Employee> employeeHashMapInput) {

        ArrayList<String> totDeviceMap = new ArrayList<>();

        for (Map.Entry<Integer, Employee> entry: employeeHashMapInput.entrySet()) {
             String firstName = entry.getValue().getFirstName();
             String lastName = entry.getValue().getLastName();
             int sumDevices = (entry.getValue().getCurrentDevices().size());

             totDeviceMap.add(firstName + " " + lastName+ ", " + " Total devices: " + sumDevices);
        }

        return new EmployeeDevicesDto(totDeviceMap);
    }

}
