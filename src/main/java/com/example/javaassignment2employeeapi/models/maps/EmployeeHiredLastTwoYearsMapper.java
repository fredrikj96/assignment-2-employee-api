package com.example.javaassignment2employeeapi.models.maps;
import com.example.javaassignment2employeeapi.models.domain.Employee;
import com.example.javaassignment2employeeapi.models.dto.EmployeeHiredLastTwoYearsDto;
import org.apache.tomcat.jni.Local;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EmployeeHiredLastTwoYearsMapper {

    public static EmployeeHiredLastTwoYearsDto employeeHiredLastTwoYearsMap (HashMap<Integer, Employee> employeeHashMapInput) {

        ArrayList<String> employeeHiredLastTwoYears = new ArrayList<>();

        for (Map.Entry<Integer, Employee> entry: employeeHashMapInput.entrySet()) {
            String firstName = entry.getValue().getFirstName();
            String lastName = entry.getValue().getLastName();
            String employeeCode = entry.getValue().getEmployeeCode();
            String employmentDate = entry.getValue().getDateEmployed();

            DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("d/M/yyyy");
            LocalDate dateEmployedLocalDate = LocalDate.parse(employmentDate, timeFormatter);
            LocalDate currentDate = LocalDate.now();

            if (dateEmployedLocalDate.isAfter(currentDate.minusYears(2)) ) {

                employeeHiredLastTwoYears.add("Name: " + firstName + " " + lastName);
                employeeHiredLastTwoYears.add("Employee Code: " + employeeCode);

            }
        }

        return new EmployeeHiredLastTwoYearsDto(employeeHiredLastTwoYears);
    }

}
