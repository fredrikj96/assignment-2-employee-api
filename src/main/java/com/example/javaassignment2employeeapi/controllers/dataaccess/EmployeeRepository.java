package com.example.javaassignment2employeeapi.controllers.dataaccess;
import com.example.javaassignment2employeeapi.models.domain.Employee;
import com.example.javaassignment2employeeapi.models.domain.LuhnValidator;
import com.example.javaassignment2employeeapi.models.dto.EmployeeDevicesDto;
import com.example.javaassignment2employeeapi.models.dto.EmployeeHappyBirthdayDto;
import com.example.javaassignment2employeeapi.models.dto.EmployeeHiredLastTwoYearsDto;
import com.example.javaassignment2employeeapi.models.maps.EmployeeDtoMap;
import com.example.javaassignment2employeeapi.models.maps.EmployeeHappyBirthdayMapper;
import com.example.javaassignment2employeeapi.models.maps.EmployeeHiredLastTwoYearsMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Component
public class EmployeeRepository implements IEmployeeRepository {

       private HashMap <Integer, Employee> employeeMap = seedEmployee();

       private HashMap <Integer, Employee> seedEmployee() {
              var employees = new HashMap<Integer, Employee>();

              employees.put(1, new Employee(1, "John", "Doe", "14/02/1980",
                      "Part-time", "Tester", "31/3/2021", new ArrayList<>(List.of(new String[]{"Dell XPS 13"})), "2018269734"));

              employees.put(2, new Employee(2, "Jane", "Doe", "17/05/1984",
                      "Full-time", "Manager", "1/12/2018", new ArrayList<>(List.of( new String[]{"AlienwareX17", "Samsung30-inchLEDMonitor"})), "1634751505"));

              employees.put(3, new Employee(3, "Sven", "Svensson", "22/03/1987",
                      "Full-time", "Developer", "15/2/2019", new ArrayList<>(List.of(new String[] {"Dell XPS 15", "Samsung 27-inch LED Monitor"})), "1507119608"));

              return employees;
       }


       @Override
       public List<Employee> getAllEmployees() {

              var employees = new ArrayList<Employee>();

              for (var employee: employeeMap.entrySet()) {
                            employees.add(employee.getValue());

              }

              return employees;
              }

       @Override
       public Employee createEmployee(Employee employee) throws Exception {
          if (employeeMap.containsKey(employee.getId())) {
                throw new Exception ("Employee already exists");
          }

          employeeMap.put(employee.getId(), employee);

          return employeeMap.get(employee.getId());

       }

    @Override
    public Employee getEmployee(int id) {
        return employeeMap.get(id);
    }



    @Override
    public Employee replaceEmployee(Employee employee) throws Exception {
      var employeeToDelete = getEmployee(employee.getId());
      employeeMap.remove(employee.getId());
      createEmployee(employee);
      return getEmployee(employee.getId());

    }

    @Override
    public Employee modifyEmployee(Employee employee) {
        var employeeToModify = getEmployee(employee.getId());

        employeeToModify.setFirstName(employee.getFirstName());
        employeeToModify.setLastName(employee.getLastName());
        employeeToModify.setDateOfBirth(employee.getDateOfBirth());
        employeeToModify.setEmploymentStatus(employee.getEmploymentStatus());
        employeeToModify.setPosition(employee.getPosition());
        employeeToModify.setDateEmployed(employee.getDateEmployed());
        employeeToModify.setCurrentDevices(employee.getCurrentDevices());
        employeeToModify.setEmployeeCode(employee.getEmployeeCode());

        return employeeToModify;

    }

    @Override
    public void deleteEmployee(int id) {

        employeeMap.remove(id);
    }

    @Override
    public boolean employeeExists(int id) {
        return employeeMap.get(id) != null;

    }


    public boolean isValidEmployee(Employee employee) {
        return employee.getId() > 0 && employee.getFirstName() != null && employee.getLastName() != null
                && employee.getDateOfBirth() != null  && employee.getEmploymentStatus() != null  &&
                employee.getPosition() != null  && employee.getDateEmployed() != null  &&
                employee.getCurrentDevices() != null  && employee.getEmployeeCode() != null;
    }

    public boolean isValidEmployee(Employee employee, int id) {
        return isValidEmployee(employee)
                && employee.getId() == id;

    }

    @Override
    public EmployeeDevicesDto getTotDevices () {

           return EmployeeDtoMap.employeeDevicesDtoMap(employeeMap);
    }

    @Override
    public EmployeeHappyBirthdayDto getFulltimeBirthday () {

           return EmployeeHappyBirthdayMapper.employeeHappyBirthdayDtoMap(employeeMap);

    }
    @Override
    public EmployeeHiredLastTwoYearsDto getEmployeeHiredLastTwoYears () {

           return EmployeeHiredLastTwoYearsMapper.employeeHiredLastTwoYearsMap(employeeMap);

    }
    @Override
    public boolean luhnValidatorCheck (String employeeCode) {
           return LuhnValidator.luhnValidator(employeeCode);

    }

}
