package com.example.javaassignment2employeeapi.controllers.dataaccess;

import com.example.javaassignment2employeeapi.models.domain.Employee;
import com.example.javaassignment2employeeapi.models.dto.EmployeeDevicesDto;
import com.example.javaassignment2employeeapi.models.dto.EmployeeHappyBirthdayDto;
import com.example.javaassignment2employeeapi.models.dto.EmployeeHiredLastTwoYearsDto;

import java.util.List;

public interface IEmployeeRepository {

    List<Employee> getAllEmployees();
   Employee createEmployee(Employee employee) throws Exception;
   Employee getEmployee (int id);
   Employee replaceEmployee(Employee employee) throws Exception;
   Employee modifyEmployee (Employee employee);
   void deleteEmployee (int id);
   boolean employeeExists(int id);
   EmployeeDevicesDto getTotDevices ();
   EmployeeHappyBirthdayDto getFulltimeBirthday ();
   EmployeeHiredLastTwoYearsDto getEmployeeHiredLastTwoYears ();
   boolean luhnValidatorCheck (String employeeCode);
}
