package com.example.javaassignment2employeeapi.controllers;

import com.example.javaassignment2employeeapi.controllers.dataaccess.EmployeeRepository;
import com.example.javaassignment2employeeapi.models.domain.Employee;
import com.example.javaassignment2employeeapi.models.dto.EmployeeDevicesDto;
import com.example.javaassignment2employeeapi.models.dto.EmployeeHappyBirthdayDto;
import com.example.javaassignment2employeeapi.models.dto.EmployeeHiredLastTwoYearsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;


    @GetMapping
    public ResponseEntity <List<Employee>> getAllEmployees () {

        return new ResponseEntity<>(employeeRepository.getAllEmployees(), HttpStatus.OK);
    }


    @GetMapping("/{id}")
    public ResponseEntity<Employee> getEmployee (@PathVariable int id) {

        if (!employeeRepository.employeeExists(id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(employeeRepository.getEmployee(id), HttpStatus.OK);

    }

    @GetMapping("/devices/summary")
    public ResponseEntity<EmployeeDevicesDto> totDevice () {

        return new ResponseEntity<>(employeeRepository.getTotDevices(), HttpStatus.OK);
    }

    @GetMapping("/birthdays")
    public ResponseEntity<EmployeeHappyBirthdayDto> fulltimeBirthdays () {

        return new ResponseEntity<>(employeeRepository.getFulltimeBirthday(), HttpStatus.OK);
    }


    @GetMapping("/new")
    public ResponseEntity<EmployeeHiredLastTwoYearsDto> hiredLastTwoYears () {

        return new ResponseEntity<>(employeeRepository.getEmployeeHiredLastTwoYears(), HttpStatus.OK);
    }


    @PostMapping
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) throws Exception {

        if(!employeeRepository.isValidEmployee(employee)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if(!employeeRepository.luhnValidatorCheck(employee.getEmployeeCode())) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(employeeRepository.createEmployee(employee), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Employee> replaceEmployee (@PathVariable int id, @RequestBody Employee employee) throws Exception {

        if(!employeeRepository.isValidEmployee(employee, id)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if (!employeeRepository.employeeExists(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        if(!employeeRepository.luhnValidatorCheck(employee.getEmployeeCode())) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        employeeRepository.replaceEmployee(employee);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);

    }

    @PatchMapping("/{id}")

    public ResponseEntity<Employee> moodifyEmployee (@PathVariable int id, @RequestBody Employee employee) {

        if(!employeeRepository.isValidEmployee(employee, id)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if (!employeeRepository.employeeExists(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        if(!employeeRepository.luhnValidatorCheck(employee.getEmployeeCode())) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        employeeRepository.modifyEmployee(employee);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Employee> deleteEmployee (@PathVariable int id) {

        if (!employeeRepository.employeeExists(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        employeeRepository.deleteEmployee(id);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);

    }
}
