package com.example.javaassignment2employeeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaAssignment2EmployeeApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaAssignment2EmployeeApiApplication.class, args);
    }

}
