package com.example.javaassignment2employeeapi.controllers.dataaccess;

import com.example.javaassignment2employeeapi.models.domain.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeRepositoryTest {
    EmployeeRepository employeeRepository;
    @BeforeEach
    void init() {
        employeeRepository = new EmployeeRepository();
    }


    @Test
    void getAllEmployees_initialInputdataExists_returnsInitialThreeEmployees() {

    //Arrange:
        int totalExpectedEmployees = 3;

    //Act:
        int actualTotalEmployees = employeeRepository.getAllEmployees().size();

    //Assert:
        assertEquals(totalExpectedEmployees, actualTotalEmployees);

    }
    @Test
    void createEmployee_validEmployee_returnsFourEmployees() throws Exception {
        Employee employeeToAdd = new Employee(4, "Johan", "Doenut", "14/02/1980",
                "Part-time", "Tester", "31/3/2021", new ArrayList<>(List.of(new String[]{"Dell XPS 13"})), "3509791590");
        int expectedTotalEmployees = 4;

        employeeRepository.createEmployee(employeeToAdd);
        int actualTotalEmployees = employeeRepository.getAllEmployees().size();

        assertEquals(expectedTotalEmployees, actualTotalEmployees);
    }

    @Test
    void createEmployee_validEmployee_returnDetailsOfEmployees() throws Exception {
        Employee employeeToAdd = new Employee(1, "John", "Doe", "14/02/1980",
                "Part-time", "Tester", "31/3/2021", new ArrayList<>(List.of(new String[]{"Dell XPS 13"})), "2018269734");

        Employee actualEmployee = employeeRepository.createEmployee(employeeToAdd);

        assertEquals(employeeToAdd, actualEmployee);
    }

    @Test
    void createEmployee_nullEmployee_throwNullException() {
        assertThrows(NullPointerException.class, () -> employeeRepository.createEmployee(null));
    }


    @Test
    void getEmployee_employeeWithIdExist_returnEmployeeWithId() {
        Employee expectedEmployee = new Employee(1, "John", "Doe", "14/02/1980",
                "Part-time", "Tester", "31/3/2021", new ArrayList<>(List.of(new String[]{"Dell XPS 13"})), "2018269734");
        int testId = 1;

        Employee actualEmployee = employeeRepository.getEmployee(testId);

        assertEquals(expectedEmployee, actualEmployee);
    }

    @Test
    void getEmployee_employeeWithIdDoesNotExist_returnNull(){
        int testId = 4;

        Employee employee = employeeRepository.getEmployee(testId);

        assertNull(employee);
    }


    @Test
    void replaceEmployee_ExistingValidEmployeeUpdate_returnDetailsOfEmployee() throws Exception {
        Employee employeeToReplace = new Employee(1, "John", "Doe", "14/02/1980",
                "Part-time", "Tester", "31/3/2021", new ArrayList<>(List.of(new String[]{"Dell XPS 13"})), "2018269734");

        Employee actualEmployee = employeeRepository.replaceEmployee(employeeToReplace);

        assertEquals(employeeToReplace, actualEmployee);
    }

    @Test
    void modifyEmployee_modifyExistingValidEmployee_returnDetailsOfEmployee() {
        Employee employeeToModify = new Employee(1, "John", "Doe", "14/02/1980",
                "Part-time", "Tester", "31/3/2021", new ArrayList<>(List.of(new String[]{"Dell XPS 13"})), "2018269734");

        Employee actualEmployee = employeeRepository.modifyEmployee(employeeToModify);

        assertEquals(employeeToModify, actualEmployee);
    }


    @Test
    void deleteEmployee_deleteEmployeeIdThree_returnSizeOfListThatEqualsTwo() {
        int testingId = 3;
        int expected = 2;

        employeeRepository.deleteEmployee(testingId);
        int actual = employeeRepository.getAllEmployees().size();

        assertEquals(expected, actual);
    }


    @Test
    void employeeExists_validInputId_returnTrue() {
        int testingId = 1;
        boolean expected = true;

        boolean actual = employeeRepository.employeeExists(testingId);

        assertEquals(actual, expected);
    }

    @Test
    void employeeExists_invalidInputId_returnFalse() {
        int testId = 8;
        boolean expected = false;

        boolean actual = employeeRepository.employeeExists(testId);

        assertEquals(actual, expected);
    }


    @Test
    void isInputValidEmployee_InputValidEmployee_returnTrue() {
        Employee employeeValidation =new Employee(1, "John", "Doe", "14/02/1980",
                "Part-time", "Tester", "31/3/2021", new ArrayList<>(List.of(new String[]{"Dell XPS 13"})), "2018269734");

        boolean actual = employeeRepository.isValidEmployee(employeeValidation);
        boolean expected = true;
        assertEquals(actual, expected);
    }

}
