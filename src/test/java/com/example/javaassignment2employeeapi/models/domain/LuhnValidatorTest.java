package com.example.javaassignment2employeeapi.models.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LuhnValidatorTest {

    @Test
    public void luhnValidator_expectedLuhnValueTrue() {
    //Arrange, Act, Assert

        //Arrange:

        String testValidNum = "9399244756";
        boolean isValid = true;

        //Act:

        boolean actual = LuhnValidator.luhnValidator(testValidNum);


        //Assert:
        assertEquals(testValidNum, actual);

    }
}